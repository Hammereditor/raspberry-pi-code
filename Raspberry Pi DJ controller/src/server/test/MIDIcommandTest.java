package server.test;
import java.util.Arrays;

import javax.sound.midi.*;

public class MIDIcommandTest {

	public static void main(String[] args) throws Exception
	{
		
        /* Create a new Sythesizer and open it. Most of 
         * the methods you will want to use to expand on this 
         * example can be found in the Java documentation here: 
         * https://docs.oracle.com/javase/7/docs/api/javax/sound/midi/Synthesizer.html
         */
		
		Receiver r = MidiSystem.getReceiver();
		System.out.println(Arrays.toString(MidiSystem.getMidiDeviceInfo()));
		MidiDevice virtualDJconnect = MidiSystem.getMidiDevice(MidiSystem.getMidiDeviceInfo()[4]);
		
		virtualDJconnect.open();
		System.out.println(virtualDJconnect.getDeviceInfo());
		System.out.println(virtualDJconnect.getMaxReceivers() + " " + virtualDJconnect.getMaxTransmitters());
		System.out.println(virtualDJconnect.getReceivers());
		System.out.println(virtualDJconnect.getTransmitters());
		
		byte[] msg = {23};
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.START, 0, 4, 93), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_ON, 0, 6, 93), System.currentTimeMillis());
		//Thread.sleep(10000000);
		
		//note value: 0-127. channel value: 0-15. number of MIDI interfaces: infinite.
		/*valid MIDI commands: 
			NOTE_ON ([ch#]-BUTTON[note#])
			NOTE_OFF ([ch#]-BUTTON[note#])
			PITCH_BEND ([ch#]-PITCH)
			CONTROL_CHANGE (multiple commands at once: {[ch#]-SLIDER[note#], [ch#]-ENCODER[note#], [ch#]-JOG[note#]}
		*/
		
		//PC is the server and RPi is the client. PC = server socket, RPi = client socket. RPi connects to the PC
		//Server should support multiple controllers at once
		//Server should support multiple MIDI receiver interfaces at once
		//1 server --> N interfaces --> N controllers
		//Multiple interfaces per controller, multiple controllers per server
		//server sees all the controllers. controller only sees itself and the server
		
		//Format of messages:
			//JSON. each message separated by a line break "\n" character
			//Client-to-server: {[command], [data]}
				//commands available: 
					//control_set: just sends the current value a control is positioned at, like a rotary knob or slider
						//data: {[device number], [value]}. [value] must be a decimal number from 0-1
						//Example: {"control_set", {3, 0.45}}
		
					//control_increment: simulates moving a control left-right or CW/CCW, like turning a jogwheel
						//data: {[device number], [value], [value description]}. [value] can be a decimal or integer
						//Example: {"control_increment", {2, 180, "degrees clockwise"}}
		
					//control_press: simulates pressing a control once, like a simple button
						//data: {[device number]}. 
						//Example: {"control_press", {4}}
		
					//control_toggle: simulates pressing a control which keeps its state. like a Caps Lock button
						//data: {[device number], [value]}. [value]: 0 = off, 1 = on
						//Example: {"control_press", {9, 1}}
		
					//control_hold_on: simulates pressing a control and holding it down. like a "shift" key on a keyboard
						//data: {[device type], [device number]}. 
						//Example: {"control_hold_on", {4}}
		
					//control_hold_off: simulates releasing the control. like a shift key
						//data: {[device number]}. 
						//Example: {"control_hold_off", {7}}
		
					//init: tells the server what controls are available and the controller's name. the server should start accepting commands after the 'init' command is sent
						//data: {[controller name], controls: [device list]}
							//controller name: the simple, common name of the controller, in terms of what the controller does
								//example: "Core Mixer", "Vinyl Turntable 2", "CD player 1". NOT "Raspberry Pi 1", "Bassel's phone", "Debian", "Nexus 6P".
							//device list: [{device 1}, {device 2}, {device 3}, ...]
								//device: {[device type], [device number], [device description]}
								//device example: {"Jogwheel", 2, "deck 2 jogwheel"}
		
					//disconnect: tells the server that the controller will now disconnect. the server can now close every interface
				
			//Server-to-client: {[command], [data]}
				//commands available: 
					//control_set: just sends the current value a control is positioned at, like a rotary knob or slider
						//data: {[device number], [value]}. [value] must be a decimal number from 0-1
						//Example: {"control_set", {31, 0.8}}
			
					//control_increment: simulates moving a control left-right or CW/CCW, like turning a jogwheel
						//data: {[device number], [value], [value description]}. [value] can be a deimal or integer
						//Example: {"control_increment", {37, 1, "beat number"}}
			
					//control_press: simulates pressing a control once, like a simple button
						//data: {[device number]}. 
						//Example: {"control_press", {8}}
			
					//control_toggle: simulates pressing a control which keeps its state. like a Caps Lock button
						//data: {[device number], [value]}. [value]: 0 = off, 1 = on
						//Example: {"control_press", {9, 0}}
			
					//control_hold_on: simulates pressing a control and holding it down. like a "shift" key on a keyboard
						//data: {[device number]}. 
						//Example: no example yet
			
					//control_hold_off: simulates releasing the control. like a shift key
						//data: {[device number]}. 
						//Example: no example yet
			
					//init: tells the client what devices the server can control and the server's name. the client should start accepting commands after the 'init' command is sent
						//data: {[controller name], controls: [device list]}
							//controller name: the simple, common name of the server, in terms of what the server does
								//example: "Virtual DJ 8.0", "Serato 1.9.1". NOT "Windows 10", "PC", "Debian", "Bassel's laptop"
							//device list: [{device 1}, {device 2}, {device 3}, ...]
								//device: {[device type], [device number], [device description]}
								//device example: {"LevelLED", 25, "deck 1 level LED"}
			
					//disconnect: tells the client that the server will now disconnect. the client can now close every interface. 
						//if the client sends 'disconnect' first, the server should send it before closing the socket
			
					
		
				//command: {[command], [device type], [device number], [action type], [value], 
		
		//mix 'Torrent' and 'Pictures in my mind' one day
		
		//Common between client and server:
			//a class called 'DeviceCommand' which represents a JSON control command
				//stores the values that the JSON command has
		
		//Client-specific design:
			//An abstract class called 'PotControl' which represents a potentiometer-based control
				//Implements the 'DeviceCommandConverter' interface
				//Calls 'send()' when the value changes
				//Abstract method: getValue(double v)
				//Extending classes: RotaryKnob, Slider, PushButton, etc. 
			//An abstract class called 'PressButtonControl' which represents a button-based control
				//Implements the 'DeviceCommandConverter' interface
				//Calls 'send()' when the value changes to positive (button is pressed once)
				//Abstract method: onPressed()
				//Extending classes: PlayPauseButton, SyncButton, PadButton, etc.
			//An abstract class called 'ToggleButtonControl' which represents a toggle button-based control
				//Implements the 'DeviceCommandConverter' interface
				//Calls 'send()' when the value changes from + to - or from - to +
				//Abstract method: onPressed()
				//Extending classes: PlayPauseButton, SyncButton, PadButton, etc.
			//An abstract class called 'HoldButtonControl' which represents a hold button-based control
				//Implements the 'DeviceCommandConverter' interface
				//Calls 'send()' when the value changes from 0 to 1 or from 1 to 0 (pressed / released, respectively)
				//Abstract method: isBeingHeld()
				//Extending classes: CueButton, ScratchButton
			
			//MCP3008 values --> control mapper --> command values --> server
		
			////////////////////////////////////////////////////
			//DO THIS AS PART III STEP 2
			//control mapper: a feature which converts a ValueChip's input values into JSON commands. uses a hashtable or something to store the mapping(s)
				//one mapping for the whole application. mappings can be saved/loaded.
		
			////////////////////////////////////////////////////
		
			//an abstract class called 'ValueChip' which models a ADC chip or any chip which reads the positions or movements of the physical controls
				//stores a list of ValueListeners and calls their 'updateValue()' method every 
				//configurable sampling rate (Hz. / times/second) and update rate (Hz. / times/second). sample rate must be a multiple of update rate
				//abstract method 'sample()' gets the values of the chip and uses them to call the updateValue() methods of the listeners
				//a class called 'MCP3008' which implements this. future chips may be added later
			//an interface called 'ValueListener' which models any class that checks the values of the controls
				//stores the device number that it is listening to
				//method updateValue() is called by the ValueChip class
				//the listener class implements this. examples of implementers: RotaryKnob, Slider, PushButton
			//a class called 'ControlValue' which represents the position/status of a control 
				//stores just a decimal number
			//an interface called 'DeviceCommandConverter' which converts a ControlValue into a DeviceCommand
				//accepts a ControlValue as input for 'updateValue()'
				//implements the 'ValueListener' interface
				//the implementer accepts the ControlValue and returns a DeviceCommand. done via the abstract method "convert()"
				//stores a 'DeviceCommandSender' instance field object and calls 'send()' on it when sending a value
				//when/at what value changes the DCC sends is determined by the implementer
			
			//an interface called 'DeviceCommandSender' which sends the JSON command to the server
				//abstract method: send()
				//class 'ConnectionMain' implements this
			//a class called 'ConnectionMain' which is the main class and connects to the server
				//implements 'DeviceCommandSender' interface
			//a saving and loading feature for the command mapper
				//stores commands in a .json file. location is inside the main config file
			//a command-line interface
				//arguments: location of configuration file, hostname and port to connect to
		
		//Server-specific design:
			
			//Many classes that implement the 'MIDIcommandConverter' interface will be created
				//Most likely scenario: One class for each set of control types
				//Example: 'CoreKnobConverter', 'JogWheelConverter', 'CoreSliderConverter', 'SamplePadConverter'
				//Each class/object would handle all decks, since this is easy to do by converting the DeviceCommand's device #
		
			//command values --> control mapper --> MIDI messages --> DJ program
		
			////////////////////////////////////////////////////
			//DO THIS AS PART III STEP 2
			//control mapper: a feature which converts the client's input messages into MIDI messages. uses a hashtable or something to store the mapping(s)
				//one mapping per controller. if multiple controllers are used at once, multiple mappings will be used at once. mappings can be saved/loaded.
				//creates multiple DeviceCommandListeners based on the 'device number' parts of the mapping hashtable
				//owns multiple DeviceCommandListeners. When a DCL receives a command, the mapper checks the hashtable and generates a MIDI command and find the right MIDIinterface
				//owns multiple MIDIreceiverInterfaces. sends the command to the correct interface
				//hashtable keys: command format strings. values: MIDIcommandFormatters
		
			/////////////////////////////////////////////////////
			
			//an interface called 'DeviceCommandListener' which models any class that wants to receive and use the value of a certain device
				//identifies the device by [device number]
				//can accept any type of command (hold_on, hold_off, press, set, ...)
				//implemented by the 'MIDIcommandConverter' class
			//an interface called 'MIDIreceiverInterface' which simulates the loopMIDI virtual receiver
				//accepts MIDI commands and throws exceptions if problems occur
			//a class called 'MIDIcommand' which models a MIDI command's 'type' and 'note' values
				//doesn't store the volume and timestamp values
			//an interface called 'MIDIcommandConverter' which converts a DeviceCommand into a MIDIcommand
				//accepts a DeviceCommand as input
				//implements the 'DeviceCommandListener' interface
				//the implementer accepts the DeviceCommand and returns a MIDIcommand. Done via the abstract method "convert()"
			//when/at what DeviceCommands the MCC sends is determined by the implementer
		
			//a class called 'ControllerSession' which represents a connected client
				//stores a list of DeviceCommandListeners
				//when JSON lines are received, creates DeviceCommand objects and sends them to the DeviceCommandListeners
			//a class called 'ConnectionMain' which is the main class
				//accepts connections from clients with a server socket
				//creates a ControllerSession and starts it on a new thread
			//a saving and loading feature for the command mapper
				//stores commands in a .json file. location is inside the main config file
			//a command-line interface
				//arguments: location of configuration file, port to listen on
		
		//Development stages:
			//Use the two test rotary knobs for all tests 
			//Part 0: common
				//1: program DeviceCommand class
			//Part I: client-side
				//1: program ControlValue, ValueChip, MCP3008, ValueListener, and ValueListenerTest classes
				//2: create a test class and run
				//3: program DeviceCommandConverter, PotControl, RotaryKnob, RotaryKnobTest classes
				//4: create a test class and run
				//5: program 'DeviceCommandSender' and 'ConnectionMain' classes and step 1 of Part II
				//6: test and run
		
			//Part II: server-side
				//1: program 'ConnectionMain' and 'ControllerSession' class and a test class which prints received JSON commands
				//2: test and run
				//3: program 'DeviceCommandListener', 'MIDIcommand', 'MIDIcommandConverter' and 'MIDIreceiverInterface' classes. make MIDIRI class print commands for now
				//4: create a quick-and-dirty implementer of MIDI CC in order to test. Test and run
				//5: program 'CoreKnobConverter' class
				//6: Test and run
		
			//Part III: extras
				//1: add the remaining control classes to the client application
				//2: program the control mapper and common configuration file code and test
				//3: working config for server
				//4: working config for client
				//5: server-->client commands (LED indicators, beat counter, etc.) and support for it
				//6: configuration web management UI for server and client
		
		/*virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_ON, 0, 26, 127), System.currentTimeMillis());
		virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_ON, 0, 25, 127), System.currentTimeMillis());
		virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_ON, 0, 24, 127), System.currentTimeMillis());
		virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_ON, 0, 23, 127), System.currentTimeMillis());
		virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_ON, 0, 22, 127), System.currentTimeMillis());
		virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_ON, 0, 20, 127), System.currentTimeMillis());
		*/
		
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_ON, 0, 60, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage., 0, 60, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_OFF, 0, 61, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.PROGRAM_CHANGE, 0, 61, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.PITCH_BEND, 0, 61, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.CHANNEL_PRESSURE, 0, 61, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.POLY_PRESSURE, 0, 61, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.CONTROL_CHANGE, 0, 21, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.SONG_POSITION_POINTER, 0, 61, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.SONG_SELECT, 0, 61, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.SYSTEM_RESET, 0, 61, 127), System.currentTimeMillis());
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.TIMING_CLOCK, 0, 61, 127), System.currentTimeMillis());
		
		/*while (true)
		{
			virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_ON, 0, 60, 93), -1);
			Thread.sleep(1000);
			virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_OFF, 0, 60, 93), -1);
			Thread.sleep(1000);
			
			//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_OFF, 0, 6, 93), System.currentTimeMillis());
			//Thread.sleep(100);
			//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage., 0, 4, 93), System.currentTimeMillis());
		}*/
		
		//Thread.sleep(1000);
		//virtualDJconnect.getReceiver().send(new ShortMessage(ShortMessage.NOTE_OFF, 0, 4, 93), System.currentTimeMillis());
		
		/*
		System.out.println(Arrays.toString(MidiSystem.getMidiDeviceInfo()));
        Synthesizer midiSynth = MidiSystem.getSynthesizer(); 
        midiSynth.open();

        //get and load default instrument and channel lists
        Instrument[] instr = midiSynth.getDefaultSoundbank().getInstruments();
        System.out.println(Arrays.toString(instr));
        MidiChannel[] mChannels = midiSynth.getChannels();

        midiSynth.loadInstrument(instr[0]);//load an instrument


        mChannels[0].noteOn(60, 100);//On channel 0, play note number 60 with velocity 100 
      */
	}

}
