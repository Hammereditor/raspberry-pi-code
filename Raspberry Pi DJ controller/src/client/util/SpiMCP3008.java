package client.util;

import java.io.IOException;

import com.pi4j.io.spi.*;

public class SpiMCP3008 
{
	private SpiDevice chip;
	private static int CHANNEL_COUNT = 8;
	
	public SpiMCP3008(SpiChannel channel) throws Exception
	{
		this(channel, SpiDevice.DEFAULT_SPI_SPEED, SpiDevice.DEFAULT_SPI_MODE); 
	}
	
	public SpiMCP3008(SpiChannel channel, int spiSpeed, SpiMode mode) throws Exception
	{
		//chip = null;
		try { chip = SpiFactory.getInstance(channel, spiSpeed, mode); } catch (Exception e) {
			throw new Exception("Error while initializing SPI chip", e);
		}
	}
	
	public int sampleChannel(int channel) throws IOException 
	{
        // 10-bit ADC MCP3008
        byte packet[] = new byte[3];
        packet[0] = 0x01;  // INIT_CMD;  // address byte
        packet[1] = (byte) ((0x08 + channel) << 4);  // singleEnded + channel
        packet[2] = 0x00;
            
        byte[] result = chip.write(packet);
        int res = ((result[1] & 0x03 ) << 8) | (result[2] & 0xff);
        return res;
    }
	
	public int[] sample() throws IOException
	{
		int[] data = new int[CHANNEL_COUNT];
		for (int i = 0; i < CHANNEL_COUNT; i++)
			data[i] = sampleChannel(i);
		return data;
	}
	
	public int[] read(int sampleCt, int sampleDelayMs) throws IOException, InterruptedException
	{
		int[] dataTotal = new int[CHANNEL_COUNT];
		
		for (int s = 0; s < sampleCt; s++)
		{
			int[] data = sample();
			for (int i = 0; i < CHANNEL_COUNT; i++)
				dataTotal[i] += data[i];
			
			Thread.sleep(sampleDelayMs);
		}
		
		for (int i = 0; i < dataTotal.length; i++)
			dataTotal[i] = (int)((dataTotal[i] * 1.0) / sampleCt);
		return dataTotal;
	}
}
