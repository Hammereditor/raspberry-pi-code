package client.util;

import java.io.IOException;

import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;
import com.pi4j.io.spi.SpiMode;

import net.hammereditor.designutilities.design.RunnableThrowableCallback;

public class MCP3008 extends ValueChip
{
	public static final int CHANNEL_COUNT = 8;
	private SpiDevice chip;
	private SpiChannel channel;

	public MCP3008(RunnableThrowableCallback cb, int sampleRateHz, int updateRateHz, SpiChannel channel, int spiSpeed, SpiMode mode) throws Exception
	{
		super(cb, sampleRateHz, updateRateHz);
		this.channel = channel;
		try { chip = SpiFactory.getInstance(channel, spiSpeed, mode); } catch (Exception e) {
			throw new Exception("Error while initializing SPI chip", e);
		}
	}
	
	public MCP3008(RunnableThrowableCallback cb, int sampleRateHz, int updateRateHz, SpiChannel channel) throws Exception
	{
		this(cb, sampleRateHz, updateRateHz, channel, SpiDevice.DEFAULT_SPI_SPEED, SpiDevice.DEFAULT_SPI_MODE);
	}

	public double[] sample() throws Exception 
	{
		double[] data = new double[CHANNEL_COUNT];
		int[] dataR = sampleAllChannels();
		
		for (int i = 0; i < CHANNEL_COUNT; i++)
			data[i] = dataR[i];
		return data;
	}
	
	private int sampleChannel(int channel) throws IOException 
	{
        // 10-bit ADC MCP3008
        byte packet[] = new byte[3];
        packet[0] = 0x01;  // INIT_CMD;  // address byte
        packet[1] = (byte) ((0x08 + channel) << 4);  // singleEnded + channel
        packet[2] = 0x00;
            
        byte[] result = chip.write(packet);
        int res = ((result[1] & 0x03 ) << 8) | (result[2] & 0xff);
        return res;
    }
	
	private int[] sampleAllChannels() throws IOException
	{
		int[] data = new int[CHANNEL_COUNT];
		for (int i = 0; i < CHANNEL_COUNT; i++)
			data[i] = sampleChannel(i);
		return data;
	}

	public int getSampleDataLength() {
		return CHANNEL_COUNT;
	}

	public String getChipInformation() {
		return "Microchip Inc. MCP3008 on SPI port " + channel.name();
	}
}
