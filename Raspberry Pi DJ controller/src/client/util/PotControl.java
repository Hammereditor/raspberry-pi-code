package client.util;

import common.ControlSetDeviceCommand;
import common.DeviceCommandSender;

/**
 * Represents a potentiometer device
 * @author Anna
 *
 */
public class PotControl extends DeviceControl
{
	private double conversionFactor;
	private double offsetFactor; //added to the returned value
	private PotControlVL valueListener;
	
	/**
	 * 
	 * @param sender
	 * @param conversionFactor The number to multiply the chip's raw output value by. 
	 */
	public PotControl(DeviceCommandSender sender, int deviceNumber, double conversionFactor, double offsetFactor, int chipValueIndex)
	{
		super(sender, deviceNumber);
		this.conversionFactor = conversionFactor;
		this.offsetFactor = offsetFactor;
		valueListener = new PotControlVL(chipValueIndex);
	}
	
	public PotControl(DeviceCommandSender sender, int deviceNumber, double conversionFactor, int chipValueIndex)
	{
		this(sender, deviceNumber, conversionFactor, 0, chipValueIndex);
	}
	
	public PotControl(DeviceCommandSender sender, int deviceNumber, int chipValueIndex)
	{
		this(sender, deviceNumber, (1.0 / 1024.0), chipValueIndex);
	}
	
	public double getConversionFactor() {
		return conversionFactor;
	}

	public void setConversionFactor(double conversionFactor) {
		this.conversionFactor = conversionFactor;
	}

	public void sendDeviceCommand() throws Exception 
	{
		double convertedValue = valueListener.getLastValue() + offsetFactor;
		convertedValue *= offsetFactor;
		ControlSetDeviceCommand c = new ControlSetDeviceCommand(super.getDeviceNumber(), convertedValue);
		
		try { super.getCommandSender().sendDeviceCommand(c); } catch (Exception e) {
			throw new Exception("Error while sending command \'" + c.getJsonCommand().toJSONString() + "\'", e);
		}
	}
	
	private class PotControlVL extends ValueListener
	{
		private double lastValue;
		
		public PotControlVL(int chipValueIndex) 
		{
			super(chipValueIndex);
		}

		public void updateValue(double value) throws Exception
		{
			if (value != lastValue)
			{
				try { sendDeviceCommand(); } catch (Exception e) {
					throw new Exception("Error while sending device command", e);
				}
			}
			
			lastValue = value;
		}
		
		protected double getLastValue() {
			return lastValue;
		}
	}
	
	public PotControlVL getValueListener() {
		return valueListener;
	}

	public void setValueListener(PotControlVL valueListener) {
		this.valueListener = valueListener;
	}
}
