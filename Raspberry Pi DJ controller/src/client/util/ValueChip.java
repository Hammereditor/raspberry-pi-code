package client.util;

import java.util.ArrayList;
import java.util.List;

import net.hammereditor.designutilities.design.RunnableThrowable;
import net.hammereditor.designutilities.design.RunnableThrowableCallback;
import net.hammereditor.designutilities.design.Stoppable;

public abstract class ValueChip extends RunnableThrowable implements Stoppable
{
	private int sampleRateHz, updateRateHz;
	private boolean stopped;
	private List<ValueListener>[] listeners; //array index corresponds to sample array index
	
	public ValueChip(RunnableThrowableCallback cb, int sampleRateHz, int updateRateHz) throws IllegalArgumentException
	{
		super(cb);
		setSampleUpdateRate(sampleRateHz, updateRateHz);
		stopped = false;
		
		listeners = new ArrayList[getSampleDataLength()];
		for (int i = 0; i < listeners.length; i++)
			listeners[i] = new ArrayList<ValueListener> ();
	}
	
	public void addValueListener(ValueListener l) throws IllegalArgumentException
	{
		if (l.getChipValueIndex() >= getSampleDataLength())
			throw new IllegalArgumentException("The chip \'" + getChipInformation() + "\' does not have a " + l.getChipValueIndex() + "th output");
		listeners[l.getChipValueIndex()].add(l);
	}
	
	public void runThread() throws Exception
	{
		while (!stopped)
		{
			double[] sampleSum = new double[getSampleDataLength()];
			int sampleCt = sampleRateHz / updateRateHz;
			
			for (int s = 0; s < sampleCt; s++)
			{
				double[] sample = sample();
				for (int i = 0; i < sampleSum.length; i++)
					sampleSum[i] += sample[i];
			}
			
			for (int i = 0; i < sampleSum.length; i++)
			{
				sampleSum[i] = sampleSum[i] / sampleCt;
				for (ValueListener l : listeners[i])
				{
					try { l.updateValue(sampleSum[i]); } catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			Thread.sleep((int)(1.0 * 1000 / 1.0 * updateRateHz));
		}
	}
	
	public void stop()
	{
		stopped = true;
	}

	public abstract double[] sample() throws Exception;
	
	public abstract int getSampleDataLength();
	
	public abstract String getChipInformation();
	
	public int getSampleRateHz() {
		return sampleRateHz;
	}

	public void setSampleUpdateRate(int sampleRateHz, int updateRateHz) throws IllegalArgumentException
	{
		if (sampleRateHz % updateRateHz != 0)
			throw new IllegalArgumentException("Sample rate \'" + sampleRateHz + "\' is not a multiple of update rate \'" + updateRateHz + "\'");
		if (sampleRateHz > 1000 || updateRateHz > 1000 )
			throw new IllegalArgumentException("Sample rate and update rate must be 1000Hz or less");
		
		this.sampleRateHz = sampleRateHz;
		this.updateRateHz = updateRateHz;
	}

	public int getUpdateRateHz() {
		return updateRateHz;
	}

	public boolean isStopped() {
		return stopped;
	}

	public List<ValueListener>[] getListeners() {
		return listeners;
	}
}
