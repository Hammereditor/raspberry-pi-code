package client.util;

import com.pi4j.io.spi.SpiChannel;
import com.pi4j.io.spi.SpiDevice;
import com.pi4j.io.spi.SpiFactory;
import com.pi4j.wiringpi.Spi;

import java.nio.ByteBuffer;
import java.io.IOException;
 
public class SpiMCP3008test {
 
    public static SpiDevice spi = null;
    public static byte INIT_CMD = (byte) 0xD0; // 11010000
    public static byte CHANNEL_COUNT = 8;
 
    public static void main(String args[]) throws Exception 
    {
    	SpiChannel spiChannelNum = SpiChannel.CS0;
    	
    	if (args.length > 0)
    	{
    		String spiChannelStr = args[0];
    		if (spiChannelStr.equalsIgnoreCase("CS0"))
    			spiChannelNum = SpiChannel.CS0;
    		else if (spiChannelStr.equalsIgnoreCase("CS1"))
    			spiChannelNum = SpiChannel.CS1;
    	}
    	
        System.out.println("<--Pi4J--> SPI test program using MCP3008 AtoD Chip. Using channel \'" + spiChannelNum.toString() + "\'");
        int[] data = new int[CHANNEL_COUNT];
    	int sampleCount = 100, sampleDelayMs = 1;
    	
    	System.out.println("Sampling " + sampleCount + " times with " + sampleDelayMs + "ms. inbetween");
    	SpiMCP3008 mcp3008 = new SpiMCP3008(spiChannelNum);

        for (int i = 0; i < CHANNEL_COUNT; i++)
    		System.out.print("CH" + i + "\t\t");
    	System.out.println();
    	System.out.println("---------------------------------------------------------------------------------------------------------------------------");
    	
        while(true) {
            data = mcp3008.read(sampleCount, sampleDelayMs);
        	print(data);
            Thread.sleep(900);
        }
    }
    
    public static void print(int[] data)
    {
    	for (int i = 0; i < CHANNEL_COUNT; i++)
    		System.out.print(data[i] + "\t\t");
    	System.out.println();
    }
}
