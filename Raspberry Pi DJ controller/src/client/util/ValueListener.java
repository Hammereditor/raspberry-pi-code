package client.util;

public abstract class ValueListener 
{
	private int chipValueIndex;
	
	public ValueListener(int chipValueIndex)
	{
		this.chipValueIndex = chipValueIndex;
	}
	
	public int getChipValueIndex()
	{
		return chipValueIndex;
	}
	
	public abstract void updateValue(double value) throws Exception;
	
	//how to create a class which can listen to multiple valuelisteners:
		//inside a DeviceCommandConverter class (like PotControl, ToggleButtonControl, etc.), create a subclass for each valuelistener
		//make the subclasses extend ValueListener and construct it with the desired chip value index
		//make the subclasses update an instance field of the DCC which stores the last chip value
		//make one of the subclasses call an update() method of the DCC which combines all the values into one command, and sends it
}
