package client.util;

import common.DeviceCommandSender;

public class RotaryKnob extends PotControl
{
	public RotaryKnob(DeviceCommandSender sender, int deviceNumber, double conversionFactor, int chipValueIndex)
	{
		super(sender, deviceNumber, conversionFactor, -55.0, chipValueIndex);
	}
}
