package client.util;

import common.DeviceCommandSender;

public abstract class DeviceControl 
{
	private int deviceNumber;
	private DeviceCommandSender commandSender;

	public DeviceControl(DeviceCommandSender sender, int deviceNumber)
	{
		this.commandSender = sender;
		this.deviceNumber = deviceNumber;
	}
	
	/**
	 * The full JSON command with the type and data
	 * @throws Exception
	 */
	public abstract void sendDeviceCommand() throws Exception;
	
	public void setCommandSender(DeviceCommandSender commandSender) {
		this.commandSender = commandSender;
	}
	
	public DeviceCommandSender getCommandSender() {
		return commandSender;
	}
	
	public int getDeviceNumber() {
		return deviceNumber;
	}
}
