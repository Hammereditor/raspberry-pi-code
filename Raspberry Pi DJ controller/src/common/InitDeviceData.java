package common;

/**
 * Stores information for one of the devices specified in the 'init' command
 * @author Anna
 *
 */
public class InitDeviceData 
{
	private String deviceType, deviceDescription;
	private int deviceNumber;
	
	public InitDeviceData(String deviceType, String deviceDescription, int deviceNumber) 
	{
		this.deviceType = deviceType;
		this.deviceDescription = deviceDescription;
		this.deviceNumber = deviceNumber;
	}
	
	public String getDeviceType() {
		return deviceType;
	}

	public String getDeviceDescription() {
		return deviceDescription;
	}

	public int getDeviceNumber() {
		return deviceNumber;
	}
}
