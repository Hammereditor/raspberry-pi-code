package common;

import org.json.simple.JSONObject;

public class ControlPressDeviceCommand extends DeviceCommand
{
	private int deviceNumber;

	public ControlPressDeviceCommand(DeviceCommandType type) throws IllegalArgumentException
	{
		super(type);
	}
	
	public ControlPressDeviceCommand(DeviceCommandType type, int deviceNumber) throws IllegalArgumentException
	{
		this(type);
		this.deviceNumber = deviceNumber;
	}
	
	public ControlPressDeviceCommand(DeviceCommandType type, JSONObject data) throws IllegalArgumentException
	{
		this(type);
		deviceNumber = (int)data.get("deviceNumber");
	}

	public int getDeviceNumber()
	{
		return deviceNumber;
	}

	public JSONObject getJsonCommandData() 
	{
		JSONObject commandDataJ = new JSONObject();
		commandDataJ.put("deviceNumber", getDeviceNumber());
		return commandDataJ;
	}

	public void commandTypeCheck(DeviceCommandType type) throws IllegalArgumentException 
	{
		if (type != DeviceCommandType.CONTROL_PRESS)
			throw new IllegalArgumentException("Command type \'" + type + "\' must be CONTROL_PRESS");
	}
}
