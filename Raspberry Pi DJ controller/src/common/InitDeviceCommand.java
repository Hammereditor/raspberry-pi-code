package common;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Represents the specific data and abilities of the 'init' command both the server and client use
 * @author Anna
 *
 */
public class InitDeviceCommand extends DeviceCommand 
{
	private String controllerName;
	private List<InitDeviceData> devices;
	
	/**
	 * Constructs an IDC with an empty device array and no controller name
	 * @param type
	 * @throws IllegalArgumentException
	 */
	public InitDeviceCommand() throws IllegalArgumentException
	{
		super(DeviceCommandType.INIT);
	}
	
	/**
	 * Constructs an IDC with an empty device array
	 * @param type
	 * @param controllerName
	 * @throws IllegalArgumentException
	 */
	public InitDeviceCommand(String controllerName) throws IllegalArgumentException
	{
		this(controllerName, new ArrayList<InitDeviceData> ());
	}
	
	/**
	 * Constructs an IDC with all data
	 * @param type
	 * @param controllerName
	 * @param devices
	 * @throws IllegalArgumentException
	 */
	public InitDeviceCommand(String controllerName, List<InitDeviceData> devices) throws IllegalArgumentException
	{
		this();
		this.controllerName = controllerName;
		this.devices = devices;
	}

	/**
	 * Constructs an IDC with all data from a JSON object
	 * @param type
	 * @param data
	 * @throws IllegalArgumentException
	 */
	public InitDeviceCommand(JSONObject data) throws IllegalArgumentException
	{
		this();
		if (!data.containsKey("controllerName") || !data.containsKey("deviceList"))
			throw new IllegalArgumentException("JSON field \'controllerName\' or \'deviceList\' missing");
		
		controllerName = (String)data.get("controllerName");
		devices = new ArrayList<InitDeviceData> ();
		JSONArray deviceListJ = (JSONArray)data.get("deviceList");

		for (int i = 0; i < deviceListJ.size(); i++)
		{
			JSONObject deviceJ = (JSONObject)deviceListJ.get(i);
			String deviceType = (String)deviceJ.get("deviceType");
			int deviceNumber = (int)deviceJ.get("deviceNumber");
			String deviceDesc = (String)deviceJ.get("deviceDescription");
			
			devices.add(new InitDeviceData(deviceType, deviceDesc, deviceNumber));
		}
	}
	
	public List<InitDeviceData> getDevices()
	{
		return devices;
	}
	
	public String getControllerName()
	{
		return controllerName;
	}

	public JSONObject getJsonCommandData() 
	{
		JSONObject commandDataJ = new JSONObject();
		commandDataJ.put("controllerName", getControllerName());
		JSONArray deviceListJ = new JSONArray();
		commandDataJ.put("deviceList", deviceListJ);
		
		for (InitDeviceData i : devices)
		{
			JSONObject deviceJ = new JSONObject();
			deviceJ.put("deviceType", i.getDeviceType());
			deviceJ.put("deviceNumber", i.getDeviceNumber());
			deviceJ.put("deviceDescription", i.getDeviceDescription());
			
			deviceListJ.add(deviceJ);
		}
		
		return commandDataJ;
	}

	public void commandTypeCheck(DeviceCommandType type) throws IllegalArgumentException 
	{
		if (type != DeviceCommandType.INIT)
			throw new IllegalArgumentException("DeviceCommandType \'" + type + "\' must be INIT");
	}
}
