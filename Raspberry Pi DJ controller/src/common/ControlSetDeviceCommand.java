package common;

import org.json.simple.JSONObject;

public class ControlSetDeviceCommand extends ControlValueDeviceCommand 
{
	public ControlSetDeviceCommand() throws IllegalArgumentException
	{
		super(DeviceCommandType.CONTROL_SET);
	}
	
	public ControlSetDeviceCommand(int deviceNumber, double value) throws IllegalArgumentException
	{
		super(DeviceCommandType.CONTROL_SET, deviceNumber, value);
		if (value < 0 || value > 1)
			throw new IllegalArgumentException("Value \'" + value + "\' must be between 0 and 1");
	}
	
	public ControlSetDeviceCommand(JSONObject data) throws IllegalArgumentException
	{
		super(DeviceCommandType.CONTROL_SET, data);
		if (super.getValue() < 0 || super.getValue() > 1)
			throw new IllegalArgumentException("Value \'" + super.getValue() + "\' must be between 0 and 1");
	}
	
	public void commandTypeCheck(DeviceCommandType type) throws IllegalArgumentException 
	{
		if (type != DeviceCommandType.CONTROL_SET)
			throw new IllegalArgumentException("Command type \'" + type + "\' must be CONTROL_SET");
	}
}
