package common;

public abstract class DeviceCommandListener 
{
	private DeviceCommandType type;

	public DeviceCommandListener(DeviceCommandType type)
	{
		this.type = type;
	}
	
	public DeviceCommandType getType() {
		return type;
	}
	
	public abstract void receiveCommand(DeviceCommand c) throws Exception;
}
