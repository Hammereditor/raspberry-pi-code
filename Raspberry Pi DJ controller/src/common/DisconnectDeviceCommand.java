package common;

import org.json.simple.JSONObject;

/**
 * The 'disconnect' command for both the server and client
 * @author Anna
 *
 */
public class DisconnectDeviceCommand extends DeviceCommand 
{
	
	public DisconnectDeviceCommand()
	{
		super(DeviceCommandType.DISCONNECT);
	}

	public JSONObject getJsonCommandData() 
	{
		return null;
	}

	public void commandTypeCheck(DeviceCommandType type) throws IllegalArgumentException 
	{
		if (type != DeviceCommandType.DISCONNECT)
			throw new IllegalArgumentException("DeviceCommandType \'" + type + "\' must be DISCONNECT");
	}
}
