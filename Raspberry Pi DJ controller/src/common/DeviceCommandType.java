package common;

/**
 * The multiple types of commands to read or control a device that can be sent
 * @author Anna
 *
 */
public enum DeviceCommandType
{
	INIT, DISCONNECT, CONTROL_SET, CONTROL_INCREMENT, CONTROL_PRESS, CONTROL_TOGGLE, CONTROL_HOLD_ON, CONTROL_HOLD_OFF;
	//more can be added later
	
	public String getJsonCommandStr()
	{
		return this.name().toLowerCase();
	}
}
