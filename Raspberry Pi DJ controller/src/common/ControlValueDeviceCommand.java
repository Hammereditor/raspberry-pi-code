package common;

import org.json.simple.JSONObject;

public class ControlValueDeviceCommand extends DeviceCommand 
{
	private int deviceNumber;
	private double value;
	
	public ControlValueDeviceCommand(DeviceCommandType type) throws IllegalArgumentException
	{
		super(type);
	}
	
	public ControlValueDeviceCommand(DeviceCommandType type, int deviceNumber, double value) throws IllegalArgumentException
	{
		this(type);
		this.deviceNumber = deviceNumber;
		this.value = value;
	}
	
	public ControlValueDeviceCommand(DeviceCommandType type, JSONObject data) throws IllegalArgumentException
	{
		this(type);
		deviceNumber = (int)data.get("deviceNumber");
		value = (double)data.get("value");
	}

	public JSONObject getJsonCommandData() 
	{
		JSONObject commandDataJ = new JSONObject();
		commandDataJ.put("deviceNumber", getDeviceNumber());
		commandDataJ.put("value", getValue());
		return commandDataJ;
	}
	
	public int getDeviceNumber() {
		return deviceNumber;
	}

	public double getValue() {
		return value;
	}

	public void commandTypeCheck(DeviceCommandType type) throws IllegalArgumentException 
	{
		if (type != DeviceCommandType.CONTROL_SET && type != DeviceCommandType.CONTROL_INCREMENT && type != DeviceCommandType.CONTROL_TOGGLE)
			throw new IllegalArgumentException("Command type \'" + type + "\' must be CONTROL_SET or CONTROL_INCREMENT or CONTROL_TOGGLE");
	}
}
