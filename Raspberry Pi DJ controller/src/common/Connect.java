package common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import net.hammereditor.designutilities.design.RunnableThrowable;
import net.hammereditor.designutilities.design.RunnableThrowableCallback;

public abstract class Connect extends RunnableThrowable implements DeviceCommandSender
{
	private Socket net;
	private BufferedReader netIn;
	private PrintWriter netOut;
	
	private List<DeviceCommandListener> commandListeners;
	
	public Connect(Socket net, RunnableThrowableCallback onError) throws Exception
	{
		this(net, onError, new ArrayList<DeviceCommandListener> ());
	}
	
	public Connect(Socket net, RunnableThrowableCallback onError, List<DeviceCommandListener> commandListeners) throws Exception
	{
		super(onError);
		netIn = new BufferedReader(new InputStreamReader(net.getInputStream()));
		netOut = new PrintWriter(net.getOutputStream(), true);
		this.commandListeners = commandListeners;
	}
	
	public void runThread() throws Exception
	{
		String line = null;
		
		try
		{
			while ((line = netIn.readLine()) != null)
			{
				JSONParser p = new JSONParser();
				
				try
				{
					JSONObject commandJ = (JSONObject)p.parse(line);
					JSONObject dataJ = (JSONObject)commandJ.get("data");
					String commandTypeS = ((String)commandJ.get("command")).toUpperCase();
					DeviceCommandType commandType = DeviceCommandType.valueOf(commandTypeS);
					
					DeviceCommand command;
					switch (commandType)
					{
						case INIT:
							command = new InitDeviceCommand(data)
					}
					
					for (DeviceCommandListener l : getListenersForCommand(commandType))
						l.receiveCommand(c);
					
				}
				catch (Exception e) {
					Exception e2 = new Exception("Error while parsing line \'" + line + "\'", e);
					e2.printStackTrace();
				}
			}
		}
		catch (Exception e) {
			throw new Exception("Error while reading next input line. Exiting...", e);
		}
	}
	
	public List<DeviceCommandListener> getListenersForCommand(DeviceCommandType t)
	{
		List<DeviceCommandListener> res = new ArrayList<DeviceCommandListener> ();
		for (DeviceCommandListener l : commandListeners)
			if (l.getType().equals(t))
				res.add(l);
		return res;
	}
	
	public void sendDeviceCommand(DeviceCommand c) throws Exception
	{
		netOut.println(c.getJsonCommand().toJSONString());
	}
	
	public List<DeviceCommandListener> getCommandListeners()
	{
		return commandListeners;
	}
	
	public void addCommandListener(DeviceCommandListener l)
	{
		commandListeners.add(l);
	}
	
	/**
	 * Initiates the disconnection process
	 */
	public void disconnectSoft()
	{
		try
		{
			DisconnectDeviceCommand ddc = new DisconnectDeviceCommand();
			sendDeviceCommand(ddc);
			//disconnectHard();
		}
		catch (Exception e) {
			Exception e2 = new Exception("Error while soft disconnecting", e);
			e2.printStackTrace();
		}
	}
		
	private void disconnectHard()
	{
		try
		{
			netOut.close();
			netIn.close();
			net.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private class DisconnectCommandListener extends DeviceCommandListener
	{
		public DisconnectCommandListener() {
			super(DeviceCommandType.DISCONNECT);
		}

		public void receiveCommand(DeviceCommand c) throws Exception 
		{
			disconnectHard();
		}
	}
}
