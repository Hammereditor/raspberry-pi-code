package common;

import org.json.simple.JSONObject;

public class ControlHoldDeviceCommand extends DeviceCommand
{	
	private int deviceNumber;
	
	public ControlHoldDeviceCommand(DeviceCommandType type) 
	{
		super(type);
	}
	
	public int getDeviceNumber()
	{
		return deviceNumber;
	}

	public JSONObject getJsonCommandData() 
	{
		JSONObject commandDataJ = new JSONObject();
		commandDataJ.put("deviceNumber", deviceNumber);
		return commandDataJ;
	}
	
	public void commandTypeCheck(DeviceCommandType type) throws IllegalArgumentException 
	{
		if (type != DeviceCommandType.CONTROL_HOLD_ON && type != DeviceCommandType.CONTROL_HOLD_OFF)
			throw new IllegalArgumentException("Command type \'" + type + "\' must be CONTROL_HOLD_ON or CONTROL_HOLD_OFF");
	}
}
