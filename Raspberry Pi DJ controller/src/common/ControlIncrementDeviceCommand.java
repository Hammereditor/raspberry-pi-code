package common;

import org.json.simple.JSONObject;

public class ControlIncrementDeviceCommand extends ControlValueDeviceCommand
{
	private String valueDescription;
	
	public ControlIncrementDeviceCommand(int deviceNumber, double value, String valueDescription) throws IllegalArgumentException
	{
		super(DeviceCommandType.CONTROL_INCREMENT, deviceNumber, value);
		this.valueDescription = valueDescription;
	}
	
	public ControlIncrementDeviceCommand(JSONObject data) throws IllegalArgumentException
	{
		super(DeviceCommandType.CONTROL_INCREMENT, data);
		valueDescription = (String)data.get("valueDescription");
	}

	public JSONObject getJsonCommandData() 
	{
		JSONObject commandDataJ = super.getJsonCommandData();
		commandDataJ.put("valueDescription", valueDescription);
		return commandDataJ;
	}
	
	public void commandTypeCheck(DeviceCommandType type) throws IllegalArgumentException 
	{
		if (type != DeviceCommandType.CONTROL_INCREMENT)
			throw new IllegalArgumentException("Command type \'" + type + "\' must be CONTROL_INCREMENT");
	}
}
