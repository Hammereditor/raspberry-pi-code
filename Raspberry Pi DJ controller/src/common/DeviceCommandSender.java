package common;

public interface DeviceCommandSender 
{
	public void sendDeviceCommand(DeviceCommand c) throws Exception;
}
