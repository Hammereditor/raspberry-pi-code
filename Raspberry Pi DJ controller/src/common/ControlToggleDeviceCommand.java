package common;

import org.json.simple.JSONObject;

public class ControlToggleDeviceCommand extends ControlValueDeviceCommand
{
	public ControlToggleDeviceCommand() throws IllegalArgumentException
	{
		super(DeviceCommandType.CONTROL_TOGGLE);
	}
	
	public ControlToggleDeviceCommand(int deviceNumber, int toggleValue) throws IllegalArgumentException
	{
		super(DeviceCommandType.CONTROL_TOGGLE, deviceNumber, toggleValue);
	}
	
	public ControlToggleDeviceCommand(JSONObject data) throws IllegalArgumentException
	{
		super(DeviceCommandType.CONTROL_TOGGLE, data);
	}

	public JSONObject getJsonCommandData() 
	{
		JSONObject commandDataJ = super.getJsonCommandData();
		commandDataJ.put("value", getToggleValue());
		return commandDataJ;
	}

	public int getToggleValue()
	{
		return (super.getValue() == 1.0 ? 1 : 0);
	}
	
	public void commandTypeCheck(DeviceCommandType type) throws IllegalArgumentException 
	{
		if (type != DeviceCommandType.CONTROL_TOGGLE)
			throw new IllegalArgumentException("Command type \'" + type + "\' must be CONTROL_TOGGLE");
	}
}
