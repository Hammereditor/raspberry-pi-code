package common;

import org.json.simple.JSONObject;

/**
 * Represents a general-purpose command which is sent between the server or client
 * @author Anna
 *
 */
public abstract class DeviceCommand 
{
	private DeviceCommandType type;
	
	public DeviceCommand(DeviceCommandType type) throws IllegalArgumentException
	{
		commandTypeCheck(type);
		this.type = type;
	}
	
	public DeviceCommandType getType() {
		return type;
	}

	/**
	 * Generates a JSONobject for the command's data, based on instance fields and other properties
	 * @return a JSON object which represents the entire command
	 */
	public abstract JSONObject getJsonCommandData();
	
	/**
	 * Generates a JSONobject for the entire command. Uses the abstract method getJsonCommandData() to generate the 'data' field
	 * @return
	 */
	public JSONObject getJsonCommand()
	{
		JSONObject commandJ = new JSONObject();
		commandJ.put("command", type.getJsonCommandStr());
		commandJ.put("data", getJsonCommandData());
		return commandJ;
	}
	
	public abstract void commandTypeCheck(DeviceCommandType type) throws IllegalArgumentException;
}
