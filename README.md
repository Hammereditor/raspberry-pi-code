# README #

Various code projects using an RPi version 2 B, in Java and maybe Python

### DJ controller ###
A DJ music mixing controller which I built using a RPi, some electronics components, and a Java client and server program on each end. The application sends MIDI commands to the DJ software on the computer. These MIDI commands can be mapped to certain actions on the DJ software.

Designed for VirtualDJ 8.0+, but can work with any program that accepts MIDI.

![IMG_20170325_201926.jpg](https://bitbucket.org/repo/kMq5AXg/images/2528266566-IMG_20170325_201926.jpg)